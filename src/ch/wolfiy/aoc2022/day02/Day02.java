package ch.wolfiy.aoc2022.day02;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Second day of the 'Advent of Code 2022' calendar.
 *
 * The total score is the sum of your scores for each round. The score for a single round is the
 * score for the shape you selected (1 for Rock, 2 for Paper, and 3 for Scissors) plus the score
 * for the outcome of the round (0 if you lost, 3 if the round was a draw, and 6 if you won).
 *
 * Even if adding points when losing is useless, I kept the code to improve readability.
 *
 * @see <a href="https://adventofcode.com/2022/day/2">Question 1</a>
 * @see <a href="https://adventofcode.com/2022/day/2#part2">Question 2</a>
 * @author wolfiy
 */
public class Day02 {

    /**
     * Score of a regular game.
     */
    private static int score = 0;
    /**
     * Score of a fixed game (task 2).
     */
    private static int fixedScore = 0;

    /**
     * Path to the strategy guide.
     * The first column is what the opponent played, the second one is what I want.
     */
    private final static String DATA_PATH = "resources/day02.txt";

    /**
     * Scores of the different shapes and outcomes.
     */
    private final static int SCORE_ROCK = 1;
    private final static int SCORE_PAPER = 2;
    private final static int SCORE_SCISSORS = 3;

    private final static int SCORE_LOSE = 0;
    private final static int SCORE_DRAW = 3;
    private final static int SCORE_WIN = 6;


    /**
     * Main function. Will run the code to get the answer to the tasks.
     *
     * @param args arguments.
     * @throws IOException if the data file does not exist.
     */
    public static void main(String[] args) throws IOException {
        FileReader rawData = new FileReader(DATA_PATH, StandardCharsets.UTF_8);
        BufferedReader buffer = new BufferedReader(rawData);
        String line;

        // Goes through each line of the data file and plays a round with the two given moves.
        while ((line = buffer.readLine()) != null) {
            String[] round = line.trim().split(" ");
            game(round[0], round[1]);
            fixedGame(round[0], round[1]);
        }

        // Question 1.
        System.out.println("Your score is:" + score);
        // Question 2.
        System.out.println("Your fixed score is: " + fixedScore);
    }

    /**
     * Used for question 1. Will compute my score for a round.
     * X: rock.
     * Y: paper.
     * Z: scissors.
     *
     * @param op the opponent's move.
     * @param me my move.
     */
    private static void game(String op, String me) {
        scoreShape(me, false);
        scoreOutcome(op, me);
    }

    /**
     * Used for question 2.
     * X: need to lose.
     * Y: need to draw.
     * Z: need to win.
     *
     * @param op the opponent's move.
     * @param need the outcome I need.
     */
    private static void fixedGame(String op, String need) {
        switch (need) {
            case "X" -> {
                switch (op) {
                    case "A" -> scoreShape("Z", true);
                    case "B" -> scoreShape("X", true);
                    case "C" -> scoreShape("Y", true);
                }
                fixedScore += SCORE_LOSE;
            }
            case "Y" -> {
                switch (op) {
                    case "A" -> scoreShape("X", true);
                    case "B" -> scoreShape("Y", true);
                    case "C" -> scoreShape("Z", true);
                }
                fixedScore += SCORE_DRAW;
            }
            case "Z" -> {
                switch (op) {
                    case "A" -> scoreShape("Y", true);
                    case "B" -> scoreShape("Z", true);
                    case "C" -> scoreShape("X", true);
                }
                fixedScore += SCORE_WIN;
            }
        }
    }

    /**
     * Gives a score based on the outcome of a round. Code is ugly but it works.
     *
     * @param op the opponent's move.
     * @param me my move.
     */
    private static void scoreOutcome(String op, String me) {
        switch (me) {
            case "X":
                switch (op) {
                    case "A" -> score += SCORE_DRAW;
                    case "B" -> score += SCORE_LOSE;
                    case "C" -> score += SCORE_WIN;
                }
                break;
            case "Y":
                switch (op) {
                    case "A" -> score += SCORE_WIN;
                    case "B" -> score += SCORE_DRAW;
                    case "C" -> score += SCORE_LOSE;
                }
                break;
            case "Z":
                switch (op) {
                    case "A" -> score += SCORE_LOSE;
                    case "B" -> score += SCORE_WIN;
                    case "C" -> score += SCORE_DRAW;
                }
                break;
        }
    }

    /**
     * Adds to the score depending on the shape.
     *
     * @param shape the chosen shape.
     * @param isFixed true if the game is fixed (2nd question).
     * @throws IllegalStateException if the shape does not exist.
     */
    private static void scoreShape(String shape, boolean isFixed) {
        var v = 0;

        switch (shape) {
            case "X" -> v += SCORE_ROCK;
            case "Y" -> v += SCORE_PAPER;
            case "Z" -> v += SCORE_SCISSORS;
            default -> throw new IllegalStateException("Unexpected value: " + shape);
        }

        if (isFixed) fixedScore += v;
        else score += v;
    }
}
