package ch.wolfiy.aoc2022.day03;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.stream.IntStream;

/**
 * Solution to the third challenge of the 'Advent of Code' 2022. For this one, I tried to use
 * streams. I have never used them much, so I got inspired by old courses examples and Simon
 * Baars'own solution to the same challenge.
 *
 * @see <a href="https://adventofcode.com/2022/day/3">Question 1</a>
 * @see <a href="https://adventofcode.com/2022/day/3#part2">Question 2</a>
 * @see
 * <a href="https://github.com/SimonBaars/AdventOfCode-Java/blob/master/src/main/java/com/sbaars/adventofcode/year22/days/Day3.java">Simon Baar's solution</a>
 * @author wolfiy
 */
public class Day03 {

    /**
     * Path to the rucksacks' data.
     */
    private final static String DATA_PATH = "resources/day03.txt";

    /**
     * Number of sacks per lines
     */
    private final static int NUMBER_OF_ITEMS = 2;

    /**
     * Priority code shifts. 'a' should be 1, 'A' should be 27.
     */
    public static final int UPPERCASE_SHIFT = 27;
    public static final int LOWERCASE_SHIFT = 1;

    /**
     * Array containing all the rucksacks in the data file.
     */
    private static final ArrayList<String> rucksacks = new ArrayList<>();

    /**
     * Main function that will run everything needed to get the answers.
     *
     * @param args arguments.
     * @throws IOException if the data file does not exist.
     */
    public static void main(String[] args) throws IOException {
        FileReader rawData = new FileReader(DATA_PATH, StandardCharsets.UTF_8);
        BufferedReader buffer = new BufferedReader(rawData);
        String line;

        while ((line = buffer.readLine()) != null) {
            String tmp = line.trim();
            rucksacks.add(tmp);
        }

        /*
         * Stream examples:
         * https://cs108.epfl.ch/archive/22/c/STRM/STRM-notes.html
         *
         * Cutting string in half:
         * https://stackoverflow.com/a/32423445
         *
         * Mapping to the priority:
         * https://github.com/SimonBaars/AdventOfCode-Java
         *
         * What should my stream do?
         * 1. Convert the rucksacks list to a stream.
         * 2. Use map to turn each item of the list into a 2-long String array containing each
         *    halves.
         * 3. Check for elements appearing in both halves.
         * 4. Get that element's priority.
         * 5. Add sum at the end to sum all priorities.
         */
        var myStream = rucksacks.stream();
        var sum = myStream.map(items -> new String[] {
                        items.substring(0, items.length()/NUMBER_OF_ITEMS),
                        items.substring(items.length()/NUMBER_OF_ITEMS)
                })
                .mapToInt(e -> getPriorityStream(e[0])
                    .filter(i -> getPriorityStream(e[1]).anyMatch(j -> j == i))
                    .findFirst()
                    .getAsInt())
                .sum();

        System.out.println("ANSWER 1: The sum of all priorities is " + sum + ".");
    }

    /**
     * Returns the priority of the given string. Uses the fact that in Java, char values store
     * the ASCII value of the character, meaning that A to Z are 41 to 90, and a to z are 97 to 122.
     *
     * My first implementation used very similar code, but instead returned an <code>int</code>. The
     * problem with that solution was that I could not use the <code>filter()</code> method on the
     * stream inside of <code>main()</code>.
     *
     * @param s a single letter string.
     * @return the priority of the string.
     * @throws IllegalStateException if the string is not one character long.
     */
    private static IntStream getPriorityStream(String s) throws IllegalStateException {
        return s.chars().map(c -> {
            if (c >= 'A' && c <= 'Z') return c - 'A' + UPPERCASE_SHIFT;
            else if (c >= 'a' && c <= 'z') return c - 'a' + LOWERCASE_SHIFT;
            else throw new IllegalArgumentException(s + " is an invalid string.");
        });
    }

    /**
     * Returns the priority of the given string. Uses the fact that in Java, char values store
     * the ASCII value of the character, meaning that A to Z are 41 to 90, and a to z are 97 to 122.
     *
     * @param s a single letter string.
     * @return the priority of the string.
     * @throws IllegalStateException if the string is not one character long.
     */
    private static int getPriority(String s) throws IllegalStateException {
        if (s.length() != 1) throw new IllegalArgumentException("Invalid string: " + s);

        int p = 0;
        char c = s.charAt(0);

        if (c >= 'A' && c<= 'Z') p = c - 'A' + 27;
        else if (c >= 'a' && c <= 'z') p = c - 'a' + 1;
        else System.out.println(c + " is invalid");

        return p;
    }
}
