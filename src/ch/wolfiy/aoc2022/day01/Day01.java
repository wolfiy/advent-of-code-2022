package ch.wolfiy.aoc2022.day01;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Solution to the first challenge of the 'Advent of Code 2022'.
 *
 * @author wolfiy
 * @see <a href="https://adventofcode.com/2022/day/1">Question 1</a>
 * @see <a href="https://adventofcode.com/2022/day/1#part2">Question 2</a>
 */
public final class Day01 {

    /**
     * Path to the file containing the calories.
     */
    private final static String DATA_PATH = "resources/day01.txt";

    /**
     * Main function. Will compute which elf has the most calorie as well as the top three.
     *
     * @param args arguments.
     * @throws IOException if the data file is not found.
     */
    public static void main(String[] args) throws IOException {
        // List with the total calories of each elf.
        List<Integer> total = new ArrayList<>();

        // Counter for the total calories of a given elf.
        int sum = 0;

        // 6.1.6
        // https://cs108.epfl.ch/archive/22/c/INOU/INOU-notes.html
        var rawData = new FileReader(DATA_PATH, UTF_8);
        var buffer = new BufferedReader(rawData);
        String line;

        // Iterate through all lines (readLine will return null at the end of the file).
        while ((line = buffer.readLine()) != null) {
            String tmp = line.trim();

            // If the line is blank, stop adding and move on to the next elf.
            if (tmp.isBlank()) {
                total.add(sum);
                sum = 0;
                continue;
            }

            // Getting the integer value of the line.
            var lineCalories = Integer.parseInt(tmp);
            sum += lineCalories;
        }

        // Question 1 answer.
        var maxCalories = Collections.max(total);
        System.out.println( "QUESTION 1\n" +
                            "The elf carrying the most calories has " +
                            maxCalories + " Cal.");

        // Separator.
        System.out.println("\n" +
                "=============================================\n\n" +
                "QUESTION 2");

        // Question 2 answer.
        // Sort all totals (from larger to smaller) and print the top three.
        // https://cs108.epfl.ch/archive/22/c/LAMB/LAMB-notes.html
        total.sort(Integer::compare);
        Collections.reverse(total);

        System.out.print("The top three elves carry respectively");
        var topThreeTotal = 0;
        for (int i = 0; i < 3; ++i) {
            var amountCarried = total.get(i);
            topThreeTotal += amountCarried;
            System.out.print(" " + amountCarried + " Cal.");
        }
        System.out.println("\nThus the total is " + topThreeTotal + " Cal.");
    }
}
